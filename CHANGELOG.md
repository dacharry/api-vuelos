# API - PRUEBA - ALMUNDO 

API desarrollada para la prueba de Almundo.com, el api se encarga de la creación y consulta de vuelos de solo ida, vuelos de ida regreso, y multitrayecto


## [1.0.0] - 2018-04-26
### API - [ESTABLE]

## Contenido 

 - EL API contiene la funcionalidad de crear vuelos simples (solo ida)
 - Los vuelos pueden ser filtrados entre solo ida, ida y regreso y multidestinos
 - El API implementan pruebas unitarias para testear las rutas
 - Implementación del desacoplamiento para proyectos escalables
