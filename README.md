# API - PRUEBA - ALMUNDO
API desarrollada para la prueba de Almundo.com, el api se encarga de la creación y consulta de vuelos de solo ida, vuelos de ida regreso, y multitrayecto

## Desarrollador
- Daniel Andres Charry P.

## Instalación
Primero se debe clonar el proyecto
<kbd>**SSH:** git@gitlab.com:dacharry/api-vuelos.git</kbd>
A continuacion entraremos a la carpeta clonada, estando dentro ejecutaremos el comando <kbd>npm i</kbd>.
Esperaremos a que instale todas las dependencias.
Finalmente ejecutaremos el comando <kbd>npm run dev</kbd>

## Configuración 
 Una vez clonado el repositorio, se debe tener cuidado al puerto por el que corre el api, por defecto es 3004.
 si se desea cambiar, modificar el archivo de variables de entorno.


