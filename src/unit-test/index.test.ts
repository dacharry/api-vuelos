import 'jest'
import request from 'supertest'
import express from 'express'
import Server from '../index'
import bodyParser from 'body-parser'
import { Routes } from '../Routes'
import { Connect } from '../Config/Connection'
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
Object.keys(Routes).forEach(key => {
    app[Routes[key].verb](Routes[key].uri, Routes[key].action)
})
Connect()
describe('Unit-test para el api de la prueba /vuelos', () => {
    it('Unit-Test para la ruta home del api...', () => {
        return request(app).get('/').expect(200).then((res) => {
            expect(res.status).toBe(200)
        })
    })
    it('Unit-Test que obtiene los los vuelos solo ida para una fecha y ciudad en especifico', () => {
        return request(app).get('/vuelos?ori=Bogota&des=Cartagena&depart=2018-04-20').then((res) => {
            expect(200).toEqual(res.status)
        })
    })
    it('Unit-Test que obtiene los los vuelos de ida y regreso para una fecha y ciudad en especifico', () => {
        return request(app)
            .get('/vuelos-ida-regreso?ori=Bogota&des=Cartagena&depart=2018-04-20&return=2018-04-06').then((res) => {
                expect(200).toEqual(res.status)
            })
    })
    it('Unit-Test que obtiene los vuelos multidestino', () => {
        return request(app).post('/vuelos-multi-destino').send({
            multi: [{ 'ori': 'Cali', 'des': 'Medellin', 'depart': '2018-04-20' }]
        }).set('Content-Type', 'application/json')
            .set('Accept', 'application/json').then((res) => {
                expect(200).toEqual(res.status)
            })
    })
    it('Unit-Test que crea un vuelo', () => {
        return request(app).post('/vuelo').send({
            'origin': {
                'iata': 'CLO',
                'name': 'Cali',
                'date': '2018-04-20T00:00:00.000Z',
                'time': '2018-04-20T12:00:00.000Z'
            },
            'destination': {
                'iata': 'MDE',
                'name': 'Medellin',
                'date': '2018-04-20T00:00:00.000Z',
                'time': '2018-04-20T09:00:00.000Z'
            },
            'price': '50000'
        }).set('Content-Type', 'application/json')
            .set('Accept', 'application/json').then((res) => {
                expect(200).toEqual(res.status)
            })
    })
})