import Sugar from 'sugar'

export const toTimeStamp = (val: any) => {
    return Sugar.Object.isDate(val) ? val.getTime() : null
}
export const toDate = (val: any) => {
    return Sugar.Date.create(val)
}
export const onlyToDate = (val: any) => {
    return Sugar.Date.format(toDate(val), '{yyyy}-{MM}-{dd}')
}
export const onlyTime = (val: any) => {
    return Sugar.Date.format(val, '{hh}:{mm}')
}