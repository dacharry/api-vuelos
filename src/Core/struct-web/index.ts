import express from 'express'
import config from 'config'
import http from 'http'

export abstract class StructWeb {
    public app: any
    private server: any
    constructor() {
        this.app = express()
    }
    public use(middlewares: any) {
        Object.keys(middlewares).forEach((key) => {
            this.app.use(middlewares[key].mount, middlewares[key].handler)
        })
        this.app.use(express.static(`${__dirname}/public`))
    }
    public routes(routes: any): void {
        Object.keys(routes).forEach((key) => {
            this.app[routes[key].verb](routes[key].uri, routes[key].action)
        })
    }
    public listen() {
        const conf = config.get('server')
        const port: number = (conf as any).port
        this.server = http.createServer(this.app)
        this.server.listen(port, () => {
            console.log(`Servidor corriendo por el puerto ${port}`)
        })
    }
}