import { Controller } from '../Controllers'

export const Routes: { [index: string]: any } = {
    Home: {
        verb: 'get',
        uri: '/',
        action: Controller.home
    },
    Flights: {
        verb: 'get',
        uri: '/vuelos',
        action: Controller.flights
    },
    FlightRound: {
        verb: 'get',
        uri: '/vuelos-ida-regreso',
        action: Controller.roundFlight
    },
    FlightMultiDest: {
        verb: 'post',
        uri: '/vuelos-multi-destino',
        action: Controller.multiDestination
    },
    CreateFlight: {
        verb: 'post',
        uri: '/vuelo',
        action: Controller.CreateFlight
    }
}