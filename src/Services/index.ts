import { Flight } from '../Models'
import { IFlight } from '../Interfaces/IFlight'
import { IFlightsService } from '../Interfaces/IFlightsService'

export const Services = {
    home: async () => {
        return 'Home'
    },
    flights: async (info: IFlightsService) => {
        try {
            const data = await Flight.find(
                {
                    'origin.name': info.ori,
                    'destination.name': info.des,
                    'origin.date': info.depart
                }
            )
            return data
        } catch (error) {
            return `Error al buscar un vuelo...${error}`
        }
    },
    roundFlight: async (info: IFlightsService) => {
        try {
            const going = await Services.flights(info)
            const back = await Flight.find(
                {
                    'origin.name': info.des,
                    'destination.name': info.ori,
                    'origin.date': info.return
                }
            )
            return { going, back }
        } catch (error) {
            return `Error al buscar un vuelo de ida y regreso...${error}`
        }
    },
    multiDestination: async (info: IFlightsService) => {
        try {
            const arr: any = {}
            for (let i = 0; i < info.length; i++) {
                const a = await Services.flights(info[i])
                arr[`flight${i + 1}`] = a
            }
            return arr
        } catch (error) {
            return `Error al buscar un vuelo multidestino...${error}`
        }
    },
    CreateFlight: async (flight: IFlight) => {
        try {
            const data = await Flight.create(Object.assign({}, flight))
            return data
        } catch (error) {
            return `Error al crear un vuelo...${error}`
        }
    }
}