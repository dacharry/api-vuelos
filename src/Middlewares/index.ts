import cors from 'cors'
import bodyParser from 'body-parser'
import config from 'config'

export const Middleware: { [index: string]: any } = {
    cors: {
        mount: '',
        handler: cors()
    },
    urlencodedParser: {
        mount: '',
        handler: bodyParser.urlencoded({ extended: false })
    },
    jsonParser: {
        mount: '',
        handler: bodyParser.json()
    }
}