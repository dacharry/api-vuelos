import { Response, Request } from 'express'
import { Services } from '../Services'

export const Controller = {
    home: async (req: Request, res: Response) => {
        const data = await Services.home()
        res.send({ data })
    },
    flights: async (req: Request, res: Response) => {
        const data = await Services.flights(req.query)
        res.send({ data })
    },
    roundFlight: async (req: Request, res: Response) => {
        const round = await Services.roundFlight(req.query)
        res.send({ round })
    },
    multiDestination: async (req: Request, res: Response) => {
        const data = await Services.multiDestination(req.body.multi)
        res.send(data)
    },
    CreateFlight: async (req: Request, res: Response) => {
        const data = await Services.CreateFlight(req.body)
        res.send({ data })
    }
}