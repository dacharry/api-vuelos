export interface IFlightsService {
    ori: string
    des: string
    depart: string
    return?: string
}