export interface IDestination {
    iata: string,
    name: string
}
export interface IFlight {
    destination: IDestination
    image: string
    price: number
    segments: [string]
}