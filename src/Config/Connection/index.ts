import mongoose from 'mongoose'

const dbUri = 'mongodb://danielch:123456@ds153869.mlab.com:53869/vuelos'
mongoose.connect(dbUri)
export const Connect = () => {
    mongoose.connection
        .on('error', () => console.log('...Error'))
        .on('disconnected', () =>  console.log('...Disconnect'))
        .once('open', () => console.log('Success Connection DB'))
}