import { Model, model } from 'mongoose'
import { FlightModel, FlightSchema } from './Schema/Flight'

export const Flight: Model<FlightModel> = model<FlightModel>('Flight', FlightSchema)