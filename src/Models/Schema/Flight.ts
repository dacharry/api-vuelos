import { Document, Schema } from 'mongoose'
import { Destination } from './Destination'
import { IFlight } from '../../Interfaces/IFlight'
import { Origin } from './Origin'

export interface FlightModel extends IFlight, Document { }
export const FlightSchema: Schema = new Schema({
    origin: {
        required: true,
        type: Origin
    },
    destination: {
        required: true,
        type: Destination
    },
    image: {
        required: true,
        type: String,
        default: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'
    },
    price: {
        required: true,
        type: Number,
        default: 0
    },
    segments: {
        type: [{
            data: { require: true, type: String }
        }]
    }
}, { timestamps: true, versionKey: false })