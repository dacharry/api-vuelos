import { Schema } from 'mongoose'

export const Origin: Schema = new Schema({
    iata: { required: true, type: String },
    name: { required: true, type: String },
    date: { required: true, type: Date },
    time: { required: true, type: Date }
})