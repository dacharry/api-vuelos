process.env.NODE_CONFIG_DIR = `${__dirname}/env`
import { StructWeb } from './Core/struct-web'
import { Connect } from './Config/Connection'
import { Middleware } from './Middlewares'
import { Routes } from './Routes'

export default class Server extends StructWeb {
    constructor() {
        super()
        Connect()
        this.use(Middleware)
        this.routes(Routes)
    }
    public static start(): Server {
        return new Server()
    }
}
Server.start().listen()
